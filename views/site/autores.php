<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Autores */
/* @var $form ActiveForm */
?>
<div class="site-autores">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'fechanacimiento') ?>
        <?= $form->field($model, 'nombre') ?>
        <?= $form->field($model, 'email') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-autores -->
